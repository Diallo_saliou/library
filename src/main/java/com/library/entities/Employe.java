package com.library.entities;


import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "employe")
//@EqualsAndHashCode(callSuper = false)
//@Getter
//@Setter
@Data
@AllArgsConstructor
@NoArgsConstructor
//@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Employe extends AbstractEntity {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String prenom;

    private String nom;

    @Column(length = 100, unique = true)
    private String cni;

    private String adresse;

    @Column(length = 100, unique = true)
    private String telephone;

    private String telephone2;

    @Column(length = 100, unique = true)
    private String email;

    /*
    public Employe() {
    }

    public Employe(Long id, String prenom, String nom, String cni, String adresse, String telephone, String telephone2, String email) {
        this.id = id;
        this.prenom = prenom;
        this.nom = nom;
        this.cni = cni;
        this.adresse = adresse;
        this.telephone = telephone;
        this.telephone2 = telephone2;
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getCni() {
        return cni;
    }

    public void setCni(String cni) {
        this.cni = cni;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getTelephone2() {
        return telephone2;
    }

    public void setTelephone2(String telephone2) {
        this.telephone2 = telephone2;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    */
}

