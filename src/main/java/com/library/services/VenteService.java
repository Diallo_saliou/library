package com.library.services;

import com.library.entities.Vente;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface VenteService {

    List<Vente> findAllVentes();

    Optional<Vente> findVenteById(Long venteId);

    Vente saveVente(Vente vente);

    Vente updateVente(Long venteId, Vente vente);

    ResponseEntity<Object> deleteVenteClient(Long id);

    int getNombreVentes(Date d1, Date d2);

    int getNumberOfVente();

    long generateNumeroVente();

    BigDecimal countSumsOfVentess();

//   .. Vente findVenteByNumeroVente(long numeroVente);
    Vente findVenteByNumeroVente(Long numeroVente);

    Vente findByStatus(String status);

    Page<Vente> findAllVenteByPageable(Pageable pageable);

    Page<Vente> findVenteByKeyWord(String mc, Pageable pageable);

    void deleteVente(Long id);

    BigDecimal sumTotalOfVenteByDay();

    BigDecimal sumTotalOfVentesByMonth();

    BigDecimal sumTotalOfVentesByYear();

    Integer countNumberOfVenteByDay();

    List<Vente> findVenteWithParticularDayAndMonth();

    List<?> countNumberTotalOfVenteByMonth();

    List<?> sumTotalOfVenteByMonth();

    List<?> sumTotalOfVenteByYears();

    List<Vente> findListVenteByEmployeId(Long empId);

}
